# README #

How to set up this app

### What is this repository for? ###

* HouseCore application for CMSC436
* Version 1.0

### How do I get set up? ###

* Clone the repo
* Enable Git version control in Android studio if it's not already set up (http://javapapers.com/android/android-studio-git-tutorial/)

### Basic Practices ###
* Each issue should have one (or more) branch(s) associated. DO NOT WORK OFF OF MASTER
* Avoid git merge. Please use pull --rebase instead. This just keeps history clean.
* Link to an issue in each pull request. Bitbucket is nice about this. If your PR is associated with issue #1 then just putting the string "#1" in your commit message will be sufficient to link to that issue. If no issue is associated with your PR then please create one.