package com.example.housecore.housecore;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.housecore.housecore.Objects.Group;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MyGroups extends AppCompatActivity
        implements SearchView.OnQueryTextListener, NavigationView.OnNavigationItemSelectedListener {
    ListView mListView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    GroupAdapter adapter;
    LoginCredentials loginCredentials;
    List<Group> groups;
    LinearLayout noGroupsPromptContainer;
    int CREATE_GROUP_REQUEST_CODE = 1;
    private String user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_groups);
        LoginCredentialsManager lcm = new LoginCredentialsManager(getApplicationContext());
        loginCredentials = lcm.getCredentials();
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.my_groups_swipe_refresh_layout);
        mListView = (ListView) findViewById(R.id.my_groups_list);
        noGroupsPromptContainer = (LinearLayout) findViewById(R.id.no_groups_prompt_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setUpGroupsUI();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        mSwipeRefreshLayout.setColorSchemeColors(R.color.colorPrimary, R.color.colorAccent, R.color.colorPrimaryDark);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Group g = (Group) parent.getItemAtPosition(position);
                Log.v("MY_GROUPS", "Clicked " + g.getGroupName());
                Intent singleGroupIntent = new Intent(MyGroups.this, SingleGroupActivity.class);
                singleGroupIntent.putExtra("groupUUID", g.getUUID());
                singleGroupIntent.putExtra("groupName", g.getGroupName());
                startActivity(singleGroupIntent);
            }
        });
        setUpGroupsUI();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*Floating Action Button Handler*/
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            //begin create group activity when FAB is pressed
            public void onClick(View view) {
                Intent createGroupIntent = new Intent(MyGroups.this, CreateGroupActivity.class);
                startActivityForResult(createGroupIntent, CREATE_GROUP_REQUEST_CODE);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        TextView nameText = (TextView) headerView.findViewById(R.id.nameText);
        TextView emailText = (TextView) headerView.findViewById(R.id.emailText);
        String name = getNameFromEmail(loginCredentials.email);
        nameText.setText(name);
        emailText.setText(loginCredentials.email);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }

    private class GroupAdapter extends ArrayAdapter<Group> {

        Context mContext;
        int layoutResourceId;
        ArrayList<Group> data;      //the original, complete data set
        ArrayList<Group> fdata;     //the filtered data
        private Filter filter;      //overrides the filter class to create custom filter

        public GroupAdapter(Context mContext, int layoutResourceId, ArrayList<Group> data) {

            super(mContext, layoutResourceId, data);

            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.data = new ArrayList<Group>(data);
            this.fdata = new ArrayList<Group>(data);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView==null){
                // inflate the layout
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }

            // object item based on the position
            Group group = fdata.get(position);

            // get the TextView and then set the text (item name) and tag (item ID) values
            TextView textViewItem = (TextView) convertView.findViewById(R.id.group_list_row_template);
            textViewItem.setText(group.getGroupName());
            textViewItem.setTag(group.getUUID());

            return convertView;
        }

        @Override
        //creates custom filter to filter groups
        public Filter getFilter()
        {
            if (filter == null)
                filter = new GroupFilter();
            return filter;
        }

        private class GroupFilter extends Filter
        {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                String prefix = constraint.toString().toLowerCase();

                if (prefix == null || prefix.length() == 0)
                {
                    ArrayList<Group> list = new ArrayList<Group>(data);     //if there is no search filter, simply use the full list of groups
                    results.values = list;
                    results.count = list.size();
                }
                else
                {
                    final ArrayList<Group> list = new ArrayList<Group>(data);
                    final ArrayList<Group> nlist = new ArrayList<Group>();
                    int count = list.size();

                    for (int i=0; i<count; i++)
                    {
                        final Group group = list.get(i);
                        final String value = group.getGroupName().toLowerCase();

                        if (value.contains(prefix))
                        {
                            nlist.add(group);
                        }
                    }
                    results.values = nlist;
                    results.count = nlist.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                fdata = (ArrayList<Group>)results.values;

                clear();
                int count = fdata.size();
                for (int i=0; i<count; i++)
                {
                    Group group = (Group)fdata.get(i);
                    add(group);
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == CREATE_GROUP_REQUEST_CODE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                Log.v("MY_GROUPS", "Result OK");
                setUpGroupsUI();
            } else if(resultCode == RESULT_CANCELED){
                Log.v("MY_GROUPS", "Result Cancelled");
            }
        }
    }

    private void setUpGroupsUI(){
        // our adapter instance
        getGroupsAssociatedWithUser();
        //Group [] groupsArray = groups.toArray(new Group[groups.size()]);
        ArrayList<Group> groupsArray = new ArrayList<Group>(groups);
        adapter = new GroupAdapter(this, R.layout.content_my_groups_item_template, groupsArray);
        mListView.setAdapter(adapter);
    }
    private void getGroupsAssociatedWithUser(){
        class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
            private Database d = new Database();
            @Override
            protected Void doInBackground(String... params) {
                groups = d.getGroupsFromEmail(params[0]);
                return null;
            }

        };
        AsyncTaskRunner a = new AsyncTaskRunner();
        try {
            a.execute(loginCredentials.email).get();
            Iterator<Group> groupIterator = groups.iterator();
            if(groups.size() == 0){
                noGroupsPromptContainer.setVisibility(View.VISIBLE);
            } else {
                noGroupsPromptContainer.setVisibility(View.GONE);
                while(groupIterator.hasNext()){
                    Log.v("MY_GROUPS", groupIterator.next().getGroupName());
                }
            }

        } catch (Exception e) {
            Log.v("MY_GROUPS", "Failed to retrieve groups");
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.group_search_menu, menu);

        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.group_search_menu);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            LoginCredentialsManager lcmMyGroups = new LoginCredentialsManager(getApplicationContext());
            lcmMyGroups.getCredentials();
            lcmMyGroups.clearCredentials();
            Intent LoginActivityIntent = new Intent(this, LoginActivity.class);
            startActivity(LoginActivityIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private String getNameFromEmail(String em) {

        class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
            private Database d = new Database();

            private String result;
            @Override
            protected Void doInBackground(String... params) {
                user = d.getNameFromEmail(params[0]);
                return null;
            }

        };
        AsyncTaskRunner a = new AsyncTaskRunner();
        try {
            a.execute(em).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return user;
    }
}
