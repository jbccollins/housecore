package com.example.housecore.housecore;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;
import java.util.UUID;

import com.example.housecore.housecore.Objects.Group;
import com.example.housecore.housecore.Objects.Item;
import com.example.housecore.housecore.Objects.Split;
import com.example.housecore.housecore.Objects.User;

/**
 * Created by Orest on 4/6/2016.
 */
public class TestSQL {

    public static void main(String[] args) {
//        getNameFromEmail("dachenzhao@yahoo.com");
//        getUsers("name", "email", "dachenzhao@yahoo.com");
//        addUser("PersonA", "PersonA@gmail.com", "fuckyou123");
//        String[] emailList = {"PersonA@gmail.com", "PersonB@gmail.com"};
//        addGroup(emailList, "PersonGroup", "PersonA@gmail.com");

//        java.util.Date dt = new java.util.Date();

//        java.text.SimpleDateFormat sdf =
//                new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//        String currentTime = sdf.format(dt);
//        addItem("PersonA@gmail.com", 1000, "Rent", "PersonB@gmail.com", "jajajhahahuuid", false, currentTime);



//        getItemFromID("09d5c8b8-c57d-4d3d-885c-9286e0ba329c");
//        validateLogin("javer", "javer", "javerbukh@gmail.com");
//        List a = getGroupsFromEmail("javerbukh@gmail.com");

        List<Item> a = getItemsFromGroupID("jajajhahahuuid");
        System.out.println("Done");
    }

    static Statement getStatement() {
        Connection con = null;
        Statement st = null;
        String url = "jdbc:mysql://sql5.freesqldatabase.com:3306/sql5115417";
        String user = "sql5115417";
        String password = "2gI59QYLa7";

        try {
            Class.forName("com.mysql.jdbc.Driver");

            con = DriverManager.getConnection(url, user, password);
            st = con.createStatement();
        } catch (ClassNotFoundException ex) {

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return st;
    }
    static void getNameFromEmail(String email) {
        Statement s = getStatement();
        ResultSet rs = null;

        String query = "SELECT name FROM sql5115417.Users WHERE email = \'" + email + "\'";
        try {
            rs = s.executeQuery(query);

            if (rs.next()) {
                System.out.println(rs.getString(1));
            }

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    static void getUsers(String a, String b, String bString) {
        Statement s = getStatement();
        ResultSet rs = null;

        String query = "SELECT " + a + " FROM sql5115417.Users WHERE " + b + " = \'" + bString + "\'";
        try {
            rs = s.executeQuery(query);

            if (rs.next()) {
                System.out.println(rs.getString(1));
            }

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }

    }

    static void addGroup (String [] useremails_in_group_array, String group_name, String creator_email) {
        Statement s = getStatement();
        ResultSet rs = null;
        UUID newGroupId = UUID.randomUUID();

        String query = "INSERT sql5115417.Groups (group_id, group_name, email, creator_email) VALUES";

        for (String email: useremails_in_group_array){
            query += " ("+ "\"" + newGroupId + "\"" + ", " + "\"" + group_name + "\"" + ", " + "\"" +email + "\"" +  ", " +"\"" + creator_email + "\"" + "),";
        }
        query = query.substring(0, query.length() -1); //removes last comma from all the values
        query += ";";
        System.out.println(query);
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }

    }


    static void addUser (String name, String userEmail, String password) {
        Statement s = getStatement();
        ResultSet rs = null;


        String query = "INSERT sql5115417.Users (name, email, password) VALUES";
        query += " ("+ "\"" + name + "\"" + ", " + "\"" + userEmail + "\"" + ", " + "\"" +password + "\"" +  ");";

        System.out.println(query);
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }

    }

    static void addItem (String creatorEmail, double amount, String itemName, String userEmail,
                         String groupUUID, boolean paid, String dateDue) {
        Statement s = getStatement();
        ResultSet rs = null;
        UUID itemUUID = UUID.randomUUID();
        Date dateCreated = new Date();
        java.sql.Timestamp dtCreated = new java.sql.Timestamp(dateCreated.getTime());
//        java.sql.Timestamp dtCreated = SQLDateTime(dateCreated.getTime());

        java.util.Date dt = new java.util.Date();

        java.text.SimpleDateFormat sdf =
                new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String currentTime = sdf.format(dt);

        String query = "INSERT sql5115417.Items (creator_email, amount, item_id, item_name," +
                "email, group_id, paid, date_created, date_due) VALUES";
        query += " ("+ "\"" + creatorEmail + "\"" + ", "  + amount + ", "   + "\"" + itemUUID + "\"" + ", "
                + "\"" + itemName + "\"" + ", "   + "\"" + userEmail + "\"" + ", " + "\"" + groupUUID + "\"" + ", " + paid + ", " +  "\"" + currentTime + "\"" + ", "
                + "\"" +  dateDue+ "\"" +  ");";

        System.out.println(query);
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }

    }

    static java.sql.Timestamp SQLDateTime(Long utilDate) {
        return new java.sql.Timestamp(utilDate);
    }

    static List<Item> getItemsFromGroupID(String groupId) {
        Statement s = getStatement();
        ResultSet rs = null;

        HashSet<String> ids = new HashSet<String>();
        ArrayList<Item> result = new ArrayList<Item>();

        String query = "SELECT item_id FROM sql5115417.Items WHERE group_id = \'" + groupId + "\'";
        try {
            rs = s.executeQuery(query);

            while (rs.next()) {
                ids.add(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }

        for (String id: ids) {
            result.add(getItemFromID(id));
        }
        return result;
    }

    static Item getItemFromID(String itemId) {
        Statement s = getStatement();
        ResultSet rs = null;

        Item i = null;

        String query = "Select *  FROM sql5115417.Items WHERE item_id = \'" + itemId + "\'";
        try {
            rs = s.executeQuery(query);

            ArrayList<Split> splits = new ArrayList<Split>();

            if(rs == null) {
                System.err.println("No item of " + itemId + " exists");
            }

            while(rs.next()) {
                Split split = new Split(rs.getDouble(2), rs.getString(9), rs.getBoolean(6));
                splits.add(split);
            }
            rs.previous();
            i = new Item(rs.getString(4), rs.getString(1), (Date)rs.getObject(8), splits, rs.getString(5), rs.getString(3));
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        return i;
    }


    static List<Group> getGroupsFromEmail(String email) {
        Statement s = getStatement();
        ResultSet rs = null;

        String query = "Select * FROM sql5115417.Groups WHERE email = \'" + email + "\'";
        List<Group> result = new ArrayList();

        try {
            rs = s.executeQuery(query);
            while (rs.next()) {
                Group g = new Group(rs.getString(1), rs.getString(2), rs.getString(4));
                result.add(g);
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        return result;
    }

    static boolean validateLogin(String name, String password, String email) {
        Statement s = getStatement();
        ResultSet rs = null;

        String query = "Select email FROM sql5115417.Users WHERE name = \'" + name + "\' AND password = \'" + password + "\' AND email = \'" + email + "\'";
        try {
            rs = s.executeQuery(query);

            // Valid login
            if (rs.next()) {
//                query = "Select * FROM sql5115417.Groups WHERE email = \'" + email + "\'";
//                rs = s.executeQuery(query);
//
//                List<String> groupIDs = new ArrayList<>();
//
//                while(rs.next()) {
//                    groupIDs.add(rs.getString(1));
//                }
                return true;
            }
            // Invalid login
            else
                return false;


        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        return false;
    }



    static void markPaid (String email, String itemId) {
        Statement s = getStatement();
        ResultSet rs = null;
        String query = "UPDATE sql5115417.Items SET paid = 1 WHERE email= \"" + email + "\" AND item_id = \"" + itemId + "\";";
//        System.out.println(query);
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }

    }


    static void addUserToGroup (String email, String groupID    ) {
        Statement s = getStatement();
        ResultSet rs = null;


        String query = "CREATE TABLE sql5115417.temp AS SELECT * FROM sql5115417.Groups WHERE group_id = \"" + groupID + "\" LIMIT 1;";
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        System.out.println(query);


        query = "UPDATE sql5115417.temp SET email = \"" + email + "\";";
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        System.out.println(query);


        query = "INSERT INTO sql5115417.Groups SELECT * from temp;";
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        System.out.println(query);


        query = "DROP TABLE sql5115417.temp;";
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        System.out.println(query);
    }


    static void removeUserFromGroup (String email, String groupID    ) {
        Statement s = getStatement();
        ResultSet rs = null;


        String query = "DELETE FROM sql5115417.Groups WHERE group_id = \"" + groupID + "\"  AND email = \"" + email + "\";";
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        System.out.println(query);


    }




}
