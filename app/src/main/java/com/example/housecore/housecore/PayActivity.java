package com.example.housecore.housecore;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.paypal.android.MEP.CheckoutButton;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalPayment;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class PayActivity extends AppCompatActivity implements OnClickListener {

    // The PayPal environment to be used - can also be ENV_NONE and ENV_LIVE.
    private static final int environment = PayPal.ENV_SANDBOX;
    // The ID of your application that you received from PayPal. This is the default sandbox ID right now.
    private static final String appID = "APP-80W284485P519543T";
    // This is passed in for the startActivityForResult() android function, the value used is up to you.
    private static final int request = 1;
    // The amount that will be paid.
    private static BigDecimal PAYMENT_AMOUNT = null;
    // The recipient of the payment.
    private static String RECIPIENT_EMAIL = null;
    private static String ITEM_ID = null;

    protected static final int INITIALIZE_SUCCESS = 0;
    protected static final int INITIALIZE_FAILURE = 1;

    // Rather than call findViewByID all the time just store commonly used views.
    LinearLayout layoutSimplePayment;
    CheckoutButton launchSimplePayment;
    LinearLayout paypalButtonWrapper;
    Button exitApp;
    TextView title;
    TextView info;
    TextView paymentRecipient;
    TextView paymentAmount;
    DecimalFormat df = new DecimalFormat("#.00");

    LoginCredentials loginCredentials;

    public static String resultTitle;
    public static String resultInfo;

    private Handler hRefresh = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case INITIALIZE_SUCCESS:
                    setupButtons();
                    break;
                case INITIALIZE_FAILURE:
                    showFailure();
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            PAYMENT_AMOUNT = new BigDecimal(extras.getString("PAYMENT_AMOUNT"));
            RECIPIENT_EMAIL = extras.getString("RECIPIENT_EMAIL");
            ITEM_ID = extras.getString("ITEM_ID");
        } else {
            hRefresh.sendEmptyMessage(INITIALIZE_FAILURE);
        }
        LoginCredentialsManager lcm = new LoginCredentialsManager(getApplicationContext());
        loginCredentials = lcm.getCredentials();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Initialization cannot be done on the main thread.
        Thread libraryInitializationThread = new Thread() {
            @Override
            public void run() {
                initLibrary();
                // The library is initialized so let's create our CheckoutButton and update the UI.
                if (PayPal.getInstance().isLibraryInitialized()) {
                    hRefresh.sendEmptyMessage(INITIALIZE_SUCCESS);
                } else {
                    hRefresh.sendEmptyMessage(INITIALIZE_FAILURE);
                }
            }
        };
        setContentView(R.layout.activity_pay);
        libraryInitializationThread.start();
        // Display the layout and setup commonly used variables.
        layoutSimplePayment = (LinearLayout)findViewById(R.id.layoutSimplePayment);
        title = (TextView)findViewById(R.id.title);
        info = (TextView)findViewById(R.id.info);
        exitApp = (Button)findViewById(R.id.exitApp);
        paypalButtonWrapper = (LinearLayout)findViewById(R.id.paypalButtonWrapper);
        paymentRecipient = (TextView)findViewById(R.id.payment_recipient);
        paymentAmount = (TextView)findViewById(R.id.payment_amount);
        setTitle("Make Payment");
    }
    private void markItemPaid(){
        class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
            private Database d = new Database();

            @Override
            protected Void doInBackground(String... params) {
                d.markPaid(loginCredentials.email, ITEM_ID);
                return null;
            }
        }

        AsyncTaskRunner a = new AsyncTaskRunner();
        try {
            a.execute().get();
        } catch (Exception e) {
            System.out.print(e.getStackTrace());
        }
    }
    public void setupButtons() {
        PayPal pp = PayPal.getInstance();
        // Get the CheckoutButton. There are five different sizes.
        // The text on the button can either be of type TEXT_PAY or TEXT_DONATE.
        launchSimplePayment = pp.getCheckoutButton(this, PayPal.BUTTON_278x43, CheckoutButton.TEXT_PAY);
        // You'll need to have an OnClickListener for the CheckoutButton.
        // For this application, PayActivity implements OnClickListener and we
        // have the onClick() method below.
        launchSimplePayment.setOnClickListener(this);
        // The CheckoutButton is an android LinearLayout so we can add it to our display like any other View.
        paypalButtonWrapper.addView(launchSimplePayment);
        info.setText("");
        info.setVisibility(View.GONE);
        paymentRecipient.setText("Payment Recipient: " + RECIPIENT_EMAIL);
        paymentAmount.setText("Payment Amount: $" + df.format(PAYMENT_AMOUNT));
    }

    public void showFailure() {
        title.setText("FAILURE");
        info.setText(R.string.pp_initialization_failure);
        title.setVisibility(View.VISIBLE);
        info.setVisibility(View.VISIBLE);
    }

    private void initLibrary() {
        // Only need to initialize the library once.
        PayPal pp = PayPal.getInstance();
        if (pp == null) {
            // Initialize PayPal
            pp = PayPal.initWithAppID(this, appID, environment);
            // Sender will pay fees. By default this is the receiver.
            // It's free within the U.S. to send money to family and friends when you use only your PayPal
            // balance or bank account, or a combination of their PayPal balance and bank account.
            // So this really shouldn't come into play in our app.
            pp.setFeesPayer(PayPal.FEEPAYER_SENDER);
            // Shipping for Peer to Peer payments is pointless.
            pp.setShippingEnabled(false);
        }
    }

    private PayPalPayment simplePayment() {
        // Create a basic PayPalPayment.
        PayPalPayment payment = new PayPalPayment();
        payment.setSubtotal(PAYMENT_AMOUNT);
        // Sets the currency type for this payment.
        payment.setCurrencyType("USD");
        // Sets the recipient for the payment. This can also be a phone number.
        payment.setRecipient(RECIPIENT_EMAIL);
        // Depending on the payment type the checkout experience can vary slightly.
        // Set this to personal to avoid fee payments.
        payment.setPaymentType(PayPal.PAYMENT_TYPE_PERSONAL);
        return payment;
    }

    @Override
    public void onClick(View v) {
        // Click handler handles both the PayPal button and the exitApp button
        if (v == launchSimplePayment) {
            // Use our helper function to create the simple payment.
            PayPalPayment payment = simplePayment();
            // Use checkout to create our Intent.
            Intent checkoutIntent = PayPal.getInstance().checkout(payment, this, new ResultDelegate());
            // Use the android's startActivityForResult() and pass in our Intent. This will start the library.
            startActivityForResult(checkoutIntent, request);
        } else if (v == exitApp) {
            Intent in = new Intent();
            in.putExtra("payment", "unpaid");
            // Setting the Requestcode 1.
            setResult(1, in);
            finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != request) {
            return;
        }
        if (PayActivity.resultTitle == "SUCCESS") {
            launchSimplePayment.setVisibility(View.GONE);
            markItemPaid();
            Intent in = new Intent();
            in.putExtra("payment", "paid");
            setResult(22, in);

        } else if (PayActivity.resultTitle == "FAILURE") {
            Intent in = new Intent();
            in.putExtra("payment", "unpaid");
            setResult(22, in);
        } else if (PayActivity.resultTitle == "CANCELED") {
            Intent in = new Intent();
            in.putExtra("payment", "unpaid");
            setResult(22, in);
        }

        launchSimplePayment.updateButton();

        title.setText(resultTitle);
        title.setVisibility(View.VISIBLE);
        info.setText(resultInfo);
        info.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent in = new Intent();
            in.putExtra("payment", "unpaid");
            setResult(1, in);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}