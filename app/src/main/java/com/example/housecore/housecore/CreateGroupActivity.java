package com.example.housecore.housecore;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.ArraySet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

public class CreateGroupActivity extends AppCompatActivity {

    private EmailValidator emailValidator = new EmailValidator();
    private LoginCredentials loginCredentials;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        LoginCredentialsManager lcm = new LoginCredentialsManager(getApplicationContext());
        loginCredentials = lcm.getCredentials();
        Log.v("EMAIL_CREDENTIAL", loginCredentials.email);
        Log.v("PASSWORD_CREDENTIAL", loginCredentials.password);
        Log.v("REMEMBER_ME_CREDENTIAL", loginCredentials.rememberMe.toString());
    }

    //checks if the group fields have been filled out properly
    private boolean isGroupValid(){
        EditText groupNameElement = (EditText) findViewById(R.id.group_name);
        ArraySet<String> userEmailsList = collectEmails();
        boolean valid = true;
        if(userEmailsList.size() == 0) {
            groupNameElement.setError(getResources().getString(R.string.error_empty_group));
            valid = false;
        } else if(userEmailsList.size() == 1 && userEmailsList.contains(loginCredentials.email)){
            groupNameElement.setError("Please add someone other than yourself to the group");
            valid = false;
        }

        if(groupNameElement.getText().toString().trim().length() == 0 ) {
            groupNameElement.setError(getResources().getString(R.string.error_field_required));
            valid = false;
        }
        return valid;
    }

    @Override
    //Handles when user selects "Create Group" button on app bar
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            default:
                onBackPressed();
                finishWithResult(RESULT_CANCELED);
                break;
        }


        super.onOptionsItemSelected(item);
        return true;
    }

    public void createGroup(View view){
        if(!isGroupValid()){
            return;
        }
        EditText groupNameElement = (EditText) findViewById(R.id.group_name);
        ArraySet<String> userEmailsList = collectEmails();
        //Make the current user part of the group
        userEmailsList.add(loginCredentials.email);
        final String[] userEmails = userEmailsList.toArray(new String[userEmailsList.size()]);
        final String groupName = groupNameElement.getText().toString();
        final String creatorEmail = loginCredentials.email;
        class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
            private Database d = new Database();
            public Boolean result;
            @Override
            protected Void doInBackground(String... params) {
                result = d.addGroup(userEmails, groupName, creatorEmail);
                return null;
            }

        };
        AsyncTaskRunner a = new AsyncTaskRunner();
        try {
            a.execute().get();
            int duration = Toast.LENGTH_LONG;
            CharSequence text;
            if (a.result == true){
                Log.v("CREATE_GROUP_ACTIVITY", "Successfully created group");
                text = "Successfully created new group " + groupName;
                Toast toast = Toast.makeText(getApplicationContext(), text, duration);
                toast.show();
                finishWithResult(RESULT_OK);
            } else {
                Log.v("CREATE_GROUP_ACTIVITY", "Failed to create group");
                text = "Failed to create group";
                Toast toast = Toast.makeText(getApplicationContext(), text, duration);
                toast.show();
            }

        } catch (InterruptedException e) {
            //TODO: Handle group createon failure
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
    //Adds another EditText for the user to add another email
    public void addEmailLine(View view){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout memberList = (LinearLayout) findViewById(R.id.member_list);

        ViewGroup buttonContainer = (ViewGroup) view.getParent();
        ViewGroup row = (ViewGroup) buttonContainer.getParent();
        EditText emailEditText = (EditText)row.findViewWithTag("email");
        String email = emailEditText.getText().toString();
        Log.d("EMAIL", email);
        if(!emailValidator.isValidEmailAddress(email)){
            emailEditText.setError(getResources().getString(R.string.error_invalid_email));
            return;
        } else  if(!emailValidator.isEmailAddressLinkedToAccount(email)){
            emailEditText.setError(getResources().getString(R.string.error_unregisterd_email));
            return;
        }
        //Remove the ability to edit text after it is validated. Deletion
        //is still possible
        emailEditText.setFocusable(false);
        //Find and show the delete_button
        buttonContainer.findViewWithTag("delete_button").setVisibility(View.VISIBLE);
        //Hide the add_button
        view.setVisibility(View.GONE);

        //Inflate and add a new list item from the item template
        View listItem = inflater.inflate(R.layout.content_create_group_item_template, null);
        memberList.addView(listItem);

        //Focus cursor on the newly created EditText
        EditText editText = (EditText)listItem.findViewWithTag("email");
        if(editText.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        Log.d("EMAILS", collectEmails().toString());
    }

    //Removes the associated list item
    public void removeEmailLine(View view) {
        ViewGroup buttonContainer = (ViewGroup) view.getParent();
        ViewGroup row = (ViewGroup) buttonContainer.getParent();
        LinearLayout memberList = (LinearLayout) findViewById(R.id.member_list);
        memberList.removeView(row);
    }

    //Gets all the email strings from the list. Use this to help populate the DB.
    //Use an ArraySet instead of a list to avoid duplicate entries
    public ArraySet<String> collectEmails(){
        LinearLayout memberList = (LinearLayout) findViewById(R.id.member_list);
        int count = memberList.getChildCount() - 1;
        ArraySet<String> emails = new ArraySet<>();
        //< not <= to exclude the last editable one
        for (int i = 0; i < count; i++) {
            View v = memberList.getChildAt(i);
            if(v instanceof LinearLayout) {
                EditText editText = (EditText) v.findViewWithTag("email");
                String emailText = editText.getText().toString();
                emails.add(emailText);
            }
        }
        return emails;
    }
    private void finishWithResult(int result) {
        setResult(result);
        finish();
    }
}
