package com.example.housecore.housecore;

/**
 * Created by jamescollins on 5/10/16.
 */
public class LoginCredentials {
    public String password;
    public String email;
    public Boolean rememberMe;

    public LoginCredentials(String email, String password, Boolean rememberMe){
        this.email = email;
        this.password = password;
        this.rememberMe = rememberMe;
    }
}
