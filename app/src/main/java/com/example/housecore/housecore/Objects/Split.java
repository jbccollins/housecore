package com.example.housecore.housecore.Objects;

/**
 * Created by Orest on 4/19/2016.
 */
public class Split {

    Double splitAmount;
    String email;
    Boolean paid;
    public String owerName;
//    UUID splitid;

    public Split(){

    }

    public Split(Double splitAmount, String email, boolean paid) {
        this.splitAmount = splitAmount;
        this.email = email;
        this.paid = paid;
    }

    public String getEmail(){
        return this.email;
    }

    public Double getSplitAmount(){
        return this.splitAmount;
    }

    public Boolean getPaid(){
        return this.paid;
    }

    @Override
    public String toString(){
        return "splitAmount: " + splitAmount.toString() + ", email: " + email + ", paid: " + paid.toString();
    }

    public void setOwerName(String owerName){
        this.owerName = owerName;
    }
}
