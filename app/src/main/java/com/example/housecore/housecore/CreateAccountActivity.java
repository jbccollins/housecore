package com.example.housecore.housecore;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.util.concurrent.ExecutionException;

/*
Allows user to create an account
 */
public class CreateAccountActivity extends AppCompatActivity {

    EditText email, name, password, confirm_password;
    boolean emailExists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
    }

    /*
    Registers the user and then leads the user to the groups page.
    Method is evoked when the register button is clicked.
    */
    public void register(View view) {
        email = (EditText) findViewById(R.id.email);
        name = (EditText) findViewById(R.id.name);
        password = (EditText) findViewById(R.id.password);
        confirm_password = (EditText) findViewById(R.id.confirm_password);

        //if form is valid, create account and then login
        if(isFormValid()) {

            class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
                private Database d = new Database();

                @Override
                protected void onPostExecute(Void v){
                    finish();
                }

                @Override
                protected Void doInBackground(String... params) {
                    d.addUser(params[0], params[1], params[2]);
                    return null;
                }

            };
            AsyncTaskRunner a = new AsyncTaskRunner();
            a.execute(name.getText().toString().trim(), email.getText().toString().trim(), password.getText().toString().trim());
        }
    }

    //checks if all contents of the form are filled out correctly
    private boolean isFormValid(){

        boolean valid = true;

        //form handling
        if(!isEmailValid(email.getText().toString())) {
            email.setError(getResources().getString(R.string.error_invalid_email));
            valid = false;
        }
        if(doesEmailExist(email.getText().toString())){
            email.setError(getResources().getString(R.string.error_email_exists));
            valid = false;
        }
        if(email.getText().toString().trim().length() == 0) {
            email.setError(getResources().getString(R.string.error_field_required));
            valid = false;
        }
        if(!isPasswordValid(password.getText().toString())) {
            password.setError(getResources().getString(R.string.error_invalid_password));
            valid = false;
        }
        if(password.getText().toString().trim().length() == 0) {
            password.setError(getResources().getString(R.string.error_field_required));
            valid = false;
        }
        if(name.getText().toString().trim().length() == 0) {
            name.setError(getResources().getString(R.string.error_field_required));
            valid = false;
        }
        if(!confirm_password.getText().toString().equals(password.getText().toString())) {
            confirm_password.setError(getResources().getString(R.string.mismatching_passwords));
            valid = false;
        }
        if(confirm_password.getText().toString().trim().length() == 0) {
            confirm_password.setError(getResources().getString(R.string.error_field_required));
            valid = false;
        }
        return valid;
    }

    //checks to see if the email is valid
    private boolean isEmailValid(String email) {
        return email.contains("@");
    }
    private boolean doesEmailExist(String email){

        class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
            private Database d = new Database();

            private String result;
            @Override
            protected Void doInBackground(String... params) {
                emailExists = d.emailExists(params[0]);
                return null;
            }

        };
        AsyncTaskRunner a = new AsyncTaskRunner();
        try {
            a.execute(email).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return emailExists;
    }
    //checks for password validity
    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }
}

