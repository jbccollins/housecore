package com.example.housecore.housecore;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by jamescollins on 5/10/16.
 */

/*
This class exists to provide activities with an easy way to access the
login credentials of the current user. The login credentials are stored in
a file. This is to avoid having to constantly pass around credentials through intents.
A SQLite DB could have served the same purpose but would be overkill.
Credentials will be separated by newlines
@Param: email
@Param: password
@Param: rememberMe (TODO: Reach goal)
*/
public class LoginCredentialsManager {
    // You should pass in the application context when possible
    private Context ctx;
    public boolean rememberMeLCM;

    public LoginCredentialsManager(Context ctx){
        this.ctx = ctx;
    }

    public LoginCredentials getCredentials() {
        String filename = ctx.getString(R.string.login_info_file_name);
        try {
            InputStream inputStream = ctx.openFileInput(filename);
            InputStreamReader inputReader = new InputStreamReader(inputStream);
            BufferedReader buffReader = new BufferedReader(inputReader);
            String email = buffReader.readLine();
            String password = buffReader.readLine();
            rememberMeLCM = Boolean.parseBoolean(buffReader.readLine());
            return new LoginCredentials(email, password, rememberMeLCM);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public boolean getRememberMe(){
        return rememberMeLCM;
    }

    public LoginCredentials setCredentials(String email, String password, Boolean rememberMe){
        FileOutputStream outputStream;
        String filename = ctx.getString(R.string.login_info_file_name);
        try {
            outputStream = ctx.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(email.getBytes());
            outputStream.write("\n".getBytes());
            outputStream.write(password.getBytes());
            outputStream.write("\n".getBytes());
            outputStream.write(rememberMe.toString().getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new LoginCredentials(email, password, rememberMe);
    }

    // Just empties the file. Use this when logging out
    public void clearCredentials(){
        FileOutputStream outputStream;
        String filename = ctx.getString(R.string.login_info_file_name);
        try {

            outputStream = ctx.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
