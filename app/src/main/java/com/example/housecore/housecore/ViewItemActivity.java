package com.example.housecore.housecore;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.housecore.housecore.Objects.Item;
import com.example.housecore.housecore.Objects.Split;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

//import android.widget.LinearLayout.LayoutParams;


/*
public class PayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
*/
public class ViewItemActivity extends AppCompatActivity {
    private String itemID = null;
    private Item item;

    ListView mListView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView creatorEmailTextView;
    TextView createdOnTextView;
    TextView dueByTextView;
    TextView totalAmountTextView;
    DecimalFormat df = new DecimalFormat("#.00");
    LoginCredentials loginCredentials;
    Double paymentAmount = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            itemID = extras.getString("itemID");
            Log.v("VIEW_ITEM", "itemID is " + itemID);
        } else {
            Log.v("VIEW_ITEM", "Failed to get extras");
        }
        LoginCredentialsManager lcm = new LoginCredentialsManager(getApplicationContext());
        loginCredentials = lcm.getCredentials();
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.view_item_swipe_refresh_layout);
        mListView = (ListView) findViewById(R.id.view_item_items_list);
        creatorEmailTextView = (TextView) findViewById(R.id.creator_email);
        createdOnTextView = (TextView) findViewById(R.id.created_on_date);
        dueByTextView = (TextView) findViewById(R.id.due_by_date);
        totalAmountTextView = (TextView) findViewById(R.id.total_amount);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getItemFromItemID();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        mSwipeRefreshLayout.setColorSchemeColors(R.color.colorPrimary, R.color.colorAccent, R.color.colorPrimaryDark);
        getItemFromItemID();
    }
    private void setUpViewItemUI(){
        double totalAmount = 0;
        setTitle(item.itemName);
        int green = ContextCompat.getColor(getBaseContext(), R.color.green);
        Split[] splits = item.splits.toArray(new Split[item.splits.size()]);
        for (int i = 0; i < splits.length; i ++){
            totalAmount += splits[i].getSplitAmount();
        }
        if(item.creatorEmail.equals(loginCredentials.email)){
            creatorEmailTextView.setText("Created By: Me");
            creatorEmailTextView.setTextColor(green);
        } else {
            creatorEmailTextView.setText("Created by: " + item.creatorName);
        }
        SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yyyy");
        String date = dt.format(item.dateCreated);
        createdOnTextView.setText("Created On: " + date);
        dueByTextView.setText("Due By: " + dt.format(item.dateDue));
        totalAmountTextView.setText("Total Amount: $" + df.format(totalAmount));
        SplitAdapter adapter = new SplitAdapter(this, R.layout.content_view_item_list_item_template, splits);
        mListView.setAdapter(adapter);
    }

    private class SplitAdapter extends ArrayAdapter<Split> {

        Context mContext;
        int layoutResourceId;
        Split data[] = null;

        public SplitAdapter(Context mContext, int layoutResourceId, Split[] data) {

            super(mContext, layoutResourceId, data);

            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.data = data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null){
                // inflate the layout
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }

            // object item based on the position
            Split split = data[position];

            TextView splitAmountTextView = (TextView) convertView.findViewById(R.id.split_amount);
            TextView paidStatusTextView = (TextView) convertView.findViewById(R.id.paid_status);
            TextView userEmailTextView = (TextView) convertView.findViewById(R.id.user_email);
            Button payNowButton = (Button) convertView.findViewById(R.id.pay_now);
            int green = ContextCompat.getColor(this.getContext(), R.color.green);
            int red = ContextCompat.getColor(this.getContext(), R.color.red);

            splitAmountTextView.setText("$" + df.format(split.getSplitAmount()));
            if(split.getPaid() == true){
                paidStatusTextView.setText("Paid");
                paidStatusTextView.setTextColor(green);
            } else {
                paidStatusTextView.setText("Unpaid");
                paidStatusTextView.setTextColor(red);
            }
            if(split.getEmail().equals(loginCredentials.email)){
                userEmailTextView.setText("Me");
                userEmailTextView.setTextColor(green);
                if(split.getPaid() == false){
                    paymentAmount = split.getSplitAmount();
                    payNowButton.setVisibility(View.VISIBLE);
                    payNowButton.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent payActivityIntent = new Intent(ViewItemActivity.this, PayActivity.class);
                            payActivityIntent.putExtra("ITEM_ID", item.itemID);
                            payActivityIntent.putExtra("PAYMENT_AMOUNT", paymentAmount.toString());
                            payActivityIntent.putExtra("RECIPIENT_EMAIL", item.creatorEmail);
                            startActivityForResult(payActivityIntent, 99);                        }
                    });
                }
            } else {
                userEmailTextView.setText(split.owerName);
            }

            return convertView;
        }
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v("VIEW_ITEM", "Request Code " + requestCode);
        if (requestCode != 99) {
            return;
        }
        getItemFromItemID();
    }

    private void getItemFromItemID(){
        class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
            private Database d = new Database();

            @Override
            protected Void doInBackground(String... params) {
                item = d.getItemWithNameFromID(params[0]);
                return null;
            }

            @Override
            protected void onPostExecute(Void v) {
                setUpViewItemUI();
            }

        };
        AsyncTaskRunner a = new AsyncTaskRunner();
        try {
            a.execute(itemID).get();
        } catch (Exception e) {
            System.out.print(e.getStackTrace());
            Log.v("SINGLE_GROUP", "Failed to retrieve items");
        }
    }
}
