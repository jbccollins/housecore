package com.example.housecore.housecore.Objects;

import com.example.housecore.housecore.TestSQL;

/**
 * Created by Orest on 4/15/2016.
 */
public class Group {

    String uuid;
    String groupName;
    String creator;

    public Group(String uuid, String groupName, String creator){
        this.uuid = uuid;
        this.groupName = groupName;
        this.creator = creator;
    }


    public String getUUID(){
        return uuid;
    }
    public String getGroupName(){
        return groupName;
    }

    public String getCreator(){
        return creator;
    }



    public void addUser(){

    }
}
