package com.example.housecore.housecore;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.housecore.housecore.Objects.Item;
import com.example.housecore.housecore.Objects.Split;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by jamescollins on 5/12/16.
 */
public class SingleGroupActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{
    String groupName, groupUUID;
    String email;
    List<Item> items;
    ListView mListView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    LoginCredentials loginCredentials;
    LinearLayout noItemsPromptContainer;
    int VIEW_ITEM_REQUEST_CODE = 3;
    ItemAdapter adapter;
    int CREATE_ITEM_REQUEST_CODE = 4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_group);
        LoginCredentialsManager lcm = new LoginCredentialsManager(getApplicationContext());
        loginCredentials = lcm.getCredentials();
        email = loginCredentials.email;         //set user email
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            groupName = extras.getString("groupName");
            groupUUID = extras.getString("groupUUID");
        } else {
            Log.v("SINGLE_GROUP", "Failed to get extras");
        }
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.single_group_swipe_refresh_layout);
        mListView = (ListView) findViewById(R.id.single_group_items_list);
        noItemsPromptContainer = (LinearLayout) findViewById(R.id.no_items_prompt_container);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setUpItemsList();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        mSwipeRefreshLayout.setColorSchemeColors(R.color.colorPrimary, R.color.colorAccent, R.color.colorPrimaryDark);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Item i = (Item) parent.getItemAtPosition(position);
                Log.v("SINGLE_GROUP", "Clicked " + i.itemName);
                Intent viewItemIntent = new Intent(SingleGroupActivity.this, ViewItemActivity.class);
                viewItemIntent.putExtra("itemID", i.itemID);
                startActivityForResult(viewItemIntent, VIEW_ITEM_REQUEST_CODE);
            }
        });
        setTitle(groupName);
        setUpItemsList();
        /*Floating Action Button Handler*/
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            //begin create group activity when FAB is pressed
            public void onClick(View view) {
                Intent createItemIntent = new Intent(SingleGroupActivity.this, CreateItemActivity.class);
                createItemIntent.putExtra("groupID", groupUUID);
                startActivityForResult(createItemIntent, CREATE_ITEM_REQUEST_CODE);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v("VIEW_ITEM", "Request Code " + requestCode);
        if (requestCode == VIEW_ITEM_REQUEST_CODE){
            setUpItemsList();
        } else if(requestCode == CREATE_ITEM_REQUEST_CODE){
            if (resultCode == RESULT_OK){
                setUpItemsList();
            } else {
                // Cancelled so do do nothing
            }
        }
    }

    private void setUpItemsList(){
        // our adapter instance
        getItemsAssociatedWithGroup();
        ArrayList<Item> itemsArray = new ArrayList<Item>(items);
        adapter = new ItemAdapter(this, R.layout.content_single_group_item_template, itemsArray);
        mListView.setAdapter(adapter);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }

    private class ItemAdapter extends ArrayAdapter<Item> {

        Context mContext;
        int layoutResourceId;
        ArrayList<Item> data;      //the original, complete data set
        ArrayList<Item> fdata;     //the filtered data
        private Filter filter;     //creates custom filter

        public ItemAdapter(Context mContext, int layoutResourceId, ArrayList<Item> data) {

            super(mContext, layoutResourceId, data);

            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.data = new ArrayList<Item>(data);
            this.fdata = new ArrayList<Item>(data);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null){
                // inflate the layout
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }

            // object item based on the position
            Item item = fdata.get(position);

            TextView itemNameTextView = (TextView) convertView.findViewById(R.id.item_name);
            TextView paidStatusTextView = (TextView) convertView.findViewById(R.id.paid_status);
            TextView creatorEmailTextView = (TextView) convertView.findViewById(R.id.creator_email);
            TextView dueDateTextView = (TextView) convertView.findViewById(R.id.due_date);

            int green = ContextCompat.getColor(this.getContext(), R.color.green);
            int red = ContextCompat.getColor(this.getContext(), R.color.red);
            int grey = ContextCompat.getColor(this.getContext(), R.color.grey);
            itemNameTextView.setText(item.itemName);
            itemNameTextView.setTag(item.itemID);

            Split userSplit = null;

            // If the current user is the creator of the item then he/she
            // cannot owe anything on it so we display owed/complete
            int numSplits = item.splits.size();
            SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yyyy");
            String date = dt.format(item.dateDue);
            dueDateTextView.setText("Due: " + date);
            if(item.creatorEmail.equals(loginCredentials.email)){
                creatorEmailTextView.setText("Creator: Me");
                creatorEmailTextView.setTextColor(green);
                if(item.allPaid == true){
                    paidStatusTextView.setText("All Paid");
                    paidStatusTextView.setTextColor(green);
                } else {
                    int numPaid = 0;
                    for(int i = 0; i < numSplits; i++){
                        if(item.splits.get(i).getPaid() == true){
                           numPaid++;
                        }
                    }
                    paidStatusTextView.setText(numPaid + " of " + numSplits + " Paid");
                    paidStatusTextView.setTextColor(red);
                }
            } else{ // Else I might owe something on this item
                creatorEmailTextView.setText("Creator: " + item.creatorName);
                for(int i = 0; i < item.splits.size(); i++){
                    if(item.splits.get(i).getEmail().equals(loginCredentials.email)){
                        userSplit = item.splits.get(i);
                        break;
                    }
                }

                // If I don't owe anything then I wont be listed in the splits so
                // userSplit will still be null
                if(userSplit == null){
                    paidStatusTextView.setText("N/A");
                    paidStatusTextView.setTextColor(grey);
                } else {
                    if(userSplit.getPaid() == true){
                        paidStatusTextView.setText("Paid");
                        paidStatusTextView.setTextColor(green);
                    } else {
                        paidStatusTextView.setText("Unpaid");
                        paidStatusTextView.setTextColor(red);
                    }
                }
            }
            return convertView;
        }

        @Override
        //creates custom filter to filter groups
        public Filter getFilter()
        {
            if (filter == null)
                filter = new ItemFilter();
            return filter;
        }
        private class ItemFilter extends Filter{

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                String prefix = constraint.toString().toLowerCase();

                if (prefix == null || prefix.length() == 0)
                {
                    ArrayList<Item> list = new ArrayList<Item>(data);     //if there is no search filter, simply use the full list of groups
                    results.values = list;
                    results.count = list.size();
                }
                else
                {
                    final ArrayList<Item> list = new ArrayList<Item>(data);
                    final ArrayList<Item> nlist = new ArrayList<Item>();
                    int count = list.size();

                    for (int i=0; i<count; i++)
                    {
                        final Item item = list.get(i);
                        final String value = item.itemName.toLowerCase();

                        if (value.contains(prefix))
                        {
                            nlist.add(item);
                        }
                    }

                    results.values = nlist;
                    results.count = nlist.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                fdata = (ArrayList<Item>)results.values;

                clear();
                int count = fdata.size();
                for (int i=0; i<count; i++)
                {
                    Item item = (Item)fdata.get(i);
                    add(item);
                }
            }
        }

    }

    private void getItemsAssociatedWithGroup(){
        class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
            private Database d = new Database();

            @Override
            protected void onPreExecute(){
                mSwipeRefreshLayout.setRefreshing(true);
            }
            @Override
            protected Void doInBackground(String... params) {
                items = d.getItemsWithNamesfromGroupID(params[0]);
                Log.v("itemslen", String.valueOf(items.size()));
                return null;
            }

            @Override
            protected void onPostExecute(Void v) {
                mSwipeRefreshLayout.setRefreshing(false);
                Iterator<Item> itemIterator = items.iterator();
                Item i;
                if(items.size() == 0){
                    noItemsPromptContainer.setVisibility(View.VISIBLE);
                    Log.v("SINGLE_GROUP", "No items in this group");

                } else {
                    noItemsPromptContainer.setVisibility(View.GONE);
                    while(itemIterator.hasNext()){
                        i = itemIterator.next();
                        Log.v("SINGLE_GROUP", "Item created by " + i.creatorName);

                        for (int j = 0; j < i.splits.size(); j++){
                            Log.v("SINGLE_GROUP", "Split " + j + " " + i.splits.get(j).toString());
                        }
                    }
                }
            }

        };
        AsyncTaskRunner a = new AsyncTaskRunner();
        try {
            a.execute(groupUUID).get();
        } catch (Exception e) {
            System.out.print(e.getStackTrace());
            Log.v("SINGLE_GROUP", "Failed to retrieve items");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.item_search_menu, menu);

        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.item_search_menu);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(this);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.item_leave_group:
                return leaveGroup();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean leaveGroup(){
        class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
            private Database d = new Database();

            private String result;
            @Override
            protected Void doInBackground(String... params) {
                d.removeUserFromGroup(params[0], params[1]);            //remove user from group
                return null;
            }

        };
        AsyncTaskRunner a = new AsyncTaskRunner();
        try {
            a.execute(email, groupUUID).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Intent myGroupsIntent = new Intent(this, MyGroups.class);
        startActivity(myGroupsIntent);
        return true;
    }
}
