package com.example.housecore.housecore;

import android.os.AsyncTask;

import java.util.concurrent.ExecutionException;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/*
* This class contains all the validation we do for emails
* */
public class EmailValidator {
    static Boolean exists;
    //Essentially just a thin wrapper around the javax.mail library
    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }
    //Custom check for email in DB
    public static boolean isEmailAddressLinkedToAccount(String email){
        class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
            private Database d = new Database();
            @Override
            protected Void doInBackground(String... params) {
                exists =  d.emailExists(params[0]);
                return null;
            }

        };
        AsyncTaskRunner a = new AsyncTaskRunner();
        try {
            a.execute(email).get();
            return exists;
        } catch (InterruptedException e) {
            //TODO: Handle group createon failure
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }
}
