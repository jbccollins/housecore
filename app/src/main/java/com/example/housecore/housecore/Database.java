package com.example.housecore.housecore;

import android.util.Log;

import com.example.housecore.housecore.Objects.Group;
import com.example.housecore.housecore.Objects.Item;
import com.example.housecore.housecore.Objects.Split;
import com.example.housecore.housecore.Objects.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by dzhao on 4/26/2016.
 */
public class Database {

    // Logs into Database
    private Statement getStatement() {
        Connection con = null;
        Statement st = null;
        String url = "jdbc:mysql://sql5.freesqldatabase.com:3306/sql5115417";
        String user = "sql5115417";
        String password = "2gI59QYLa7";

        try {
            Class.forName("com.mysql.jdbc.Driver");

            con = DriverManager.getConnection(url, user, password);
            st = con.createStatement();
        } catch (ClassNotFoundException ex) {

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return st;
    }

    // Users
    public String getNameFromEmail(String email) {
        Statement s = getStatement();
        ResultSet rs = null;

        String query = "SELECT name FROM sql5115417.Users WHERE email = \'" + email + "\'";
        try {
            rs = s.executeQuery(query);

            if (rs.next()) {
                return rs.getString(1);
            }
            else{
                return null;
            }

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        return null;
    }

    // Users:   Get User data a from b
    public void getUsers(String a, String b, String bString) {
        Statement s = getStatement();
        ResultSet rs = null;

        String query = "SELECT " + a + " FROM sql5115417.Users WHERE " + b + " = \'" + bString + "\'";
        try {
            rs = s.executeQuery(query);

            if (rs.next()) {
                System.out.println(rs.getString(1));
            }

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    // Groups
    public Boolean addGroup (String [] useremails_in_group_array, String group_name, String creator_email) {
        Statement s = getStatement();
        ResultSet rs = null;
        UUID newGroupId = UUID.randomUUID();

        String query = "INSERT sql5115417.Groups (group_id, group_name, email, creator_email) VALUES";

        for (String email: useremails_in_group_array){
            query += " ("+ "\"" + newGroupId + "\"" + ", " + "\"" + group_name + "\"" + ", " + "\"" +email + "\"" +  ", " +"\"" + creator_email + "\"" + "),";
        }
        query = query.substring(0, query.length() -1); //removes last comma from all the values
        query += ";";
        System.out.println(query);
        try {
            s.executeUpdate(query);
            return true;
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
            return false;
        }

    }

    //Users
    public void addUser (String name, String userEmail, String password) {
        Statement s = getStatement();
        ResultSet rs = null;


        String query = "INSERT sql5115417.Users (name, email, password) VALUES";
        query += " ("+ "\"" + name + "\"" + ", " + "\"" + userEmail + "\"" + ", " + "\"" +password + "\"" +  ");";

        System.out.println(query);
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    //Items
    public void addItem (String creatorEmail, double amount, String itemName, String userEmail,
                         String groupUUID, boolean paid, String itemUUID, String dateDue) {

        if(amount != 0) {
            Statement s = getStatement();
            ResultSet rs = null;
            Date dateCreated = new Date();
            java.sql.Timestamp dtCreated = new java.sql.Timestamp(dateCreated.getTime());

            java.util.Date dt = new java.util.Date();

            java.text.SimpleDateFormat sdf =
                    new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            String currentTime = sdf.format(dt);

            String query = "INSERT sql5115417.Items (creator_email, amount, item_id, item_name," +
                    "email, group_id, paid, date_created, date_due) VALUES";
            query += " (" + "\"" + creatorEmail + "\"" + ", " + amount + ", " + "\"" + itemUUID + "\"" + ", "
                    + "\"" + itemName + "\"" + ", " + "\"" + userEmail + "\"" + ", " + "\"" + groupUUID + "\"" + ", " + paid + ", " + "\"" + currentTime + "\"" + ", "
                    + "\"" + dateDue + "\"" + ");";

            System.out.println(query);
            try {
                s.executeUpdate(query);

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(TestSQL.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }

    //Items
    public void addMultipleItems (String creatorEmail, ArrayList<Double>amount, String itemName, ArrayList<String> userEmail,
                         String groupUUID, boolean paid, String itemUUID, String dateDue) {


            Statement s = getStatement();
            ResultSet rs = null;
            Date dateCreated = new Date();
            java.sql.Timestamp dtCreated = new java.sql.Timestamp(dateCreated.getTime());

            java.util.Date dt = new java.util.Date();

            java.text.SimpleDateFormat sdf =
                    new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            String currentTime = sdf.format(dt);

            String query = "INSERT sql5115417.Items (creator_email, amount, item_id, item_name," +
                    "email, group_id, paid, date_created, date_due) VALUES";
            for(int i = 0; i < userEmail.size(); i++) {
                if(amount.get(i) > 0) {
                    query += " (" + "\"" + creatorEmail + "\"" + ", " + amount.get(i) + ", " + "\"" + itemUUID + "\"" + ", "
                            + "\"" + itemName + "\"" + ", " + "\"" + userEmail.get(i) + "\"" + ", " + "\"" + groupUUID + "\"" + ", " + paid + ", " +
                            "\"" + currentTime + "\"" + ", " + "\"" + dateDue + "\"" + "), ";
                }
            }

            query = query.substring(0, query.length()-2);
            query = query + ";";
            System.out.println(query);
            try {
                s.executeUpdate(query);

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(TestSQL.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }

    }
    private java.sql.Timestamp SQLDateTime(Long utilDate) {
        return new java.sql.Timestamp(utilDate);
    }

    //Items
    public Item getItemFromID(String itemId) {
        Statement s = getStatement();
        ResultSet rs = null;

        Item i = null;

        String query = "Select *  FROM sql5115417.Items WHERE item_id = \'" + itemId + "\'";

        try {
            rs = s.executeQuery(query);

            ArrayList<Split> splits = new ArrayList<Split>();

            if(rs == null) {
                System.err.println("No item of " + itemId + " exists");
            }

            while(rs.next()) {
                Split split = new Split(rs.getDouble(2), rs.getString(9), rs.getBoolean(6));
                splits.add(split);
            }
            rs.previous();
            i = new Item(rs.getString(4), rs.getString(1), (Date)rs.getObject(8), splits, rs.getString(5), rs.getString(3));
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        return i;
    }


    //Items
    public Item getItemWithNameFromID(String itemId) {
        Statement s = getStatement();
        ResultSet rs = null;

        Item i = null;

        String query = "Select b.name, a.* from (Select y.name, x.* from (Select * from Items where item_id = \""+ itemId +"\") as x inner join (Select * from Users) as y on x.email = y.email) as a inner join (Select r.name, q.* from (Select * from Items where item_id = \""+ itemId +"\") as q inner join (Select * from Users) as r on q.creator_email = r.email) as b on a.email = b.email;";

        System.out.println(query);
        try {
            rs = s.executeQuery(query);

            ArrayList<Split> splits = new ArrayList<Split>();

            if(rs == null) {
                System.err.println("No item of " + itemId + " exists");
            }

            while(rs.next()) {
                Split split = new Split(rs.getDouble(4), rs.getString(11), rs.getBoolean(8));
                split.setOwerName(rs.getString(2));
                splits.add(split);
            }
            rs.previous();
            i = new Item(rs.getString(6), rs.getString(3), (Date)rs.getObject(10), splits, rs.getString(7), rs.getString(5));
            i.setCreatorName(rs.getString(1));
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        return i;
    }




    public ArrayList<Item> getItemsWithNamesfromGroupID(String groupId) {
        Statement s = getStatement();
        ResultSet rs = null;


        ArrayList<Item> result = new ArrayList<Item>();
        ArrayList<Item> itemsList = new ArrayList<Item>();
        String query = "Select y.name, x.* from (Select * from Items where item_id in(SELECT item_id FROM sql5115417.Items WHERE group_id = \""+groupId+ "\") ORDER BY date_created DESC) as x inner join (Select * from Users) as y on x.creator_email = y.email;";
        System.out.println(query);
        try {
            rs = s.executeQuery(query);
            rs.next();
            String currentID = rs.getString(4);
            rs.previous();
            ArrayList<Split> currSplitList = new ArrayList<Split>();
            while (rs.next()) {
                //(1)name,(2)creator_email,(3)amount,(4)item_id,(5)item_name,(6)group_id,(7)paid,(8)date_created,(9)date_due,(10)email

                if (!rs.getString(4).equals(currentID)) {
                    rs.previous();
                    Item newItem = new Item(rs.getString(5), rs.getString(2), (Date)rs.getObject(9), currSplitList,rs.getString(6), rs.getString(4));
                    newItem.setCreatorName(rs.getString(1));
                    itemsList.add(newItem);
                    currSplitList = new ArrayList<Split>();
                    rs.next();
                }
                currentID = rs.getString(4);
                Split newSplit = new Split(rs.getDouble(3), rs.getString(10), rs.getBoolean(7));
                currSplitList.add(newSplit);
            }
            rs.previous();
            Item newItem = new Item(rs.getString(5), rs.getString(2), (Date)rs.getObject(9), currSplitList,rs.getString(6), rs.getString(4));
            newItem.setCreatorName(rs.getString(1));
            itemsList.add(newItem);
            currSplitList = new ArrayList<Split>();
            rs.next();
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }

        return itemsList;
    }
    //Items
    public List<Item> getItemsFromGroupID(String groupId) {
        Statement s = getStatement();
        ResultSet rs = null;

        HashSet<String> ids = new HashSet<String>();
        ArrayList<Item> result = new ArrayList<Item>();

        String query = "SELECT item_id FROM sql5115417.Items WHERE group_id = \'" + groupId + "\'";
        try {
            rs = s.executeQuery(query);

            while (rs.next()) {
                ids.add(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }

        for (String id: ids) {
            result.add(getItemFromID(id));
        }
        return result;
    }

    //Groups
    public List<Group> getGroupsFromEmail(String email) {
        Statement s = getStatement();
        ResultSet rs = null;

        String query = "Select * FROM sql5115417.Groups WHERE email = \'" + email + "\'";
        List<Group> result = new ArrayList();

        try {
            rs = s.executeQuery(query);
            while (rs.next()) {
                Group g = new Group(rs.getString(1), rs.getString(2), rs.getString(4));
                result.add(g);
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        return result;
    }

    //Users
    public boolean emailExists(String email) {
        Statement s = getStatement();
        ResultSet rs = null;
        String query = "Select email FROM sql5115417.Users WHERE email = \'" + email + "\'";
        Log.v("DATABASE", query);
        try {
            rs = s.executeQuery(query);
            if (rs.next()) {
                return true;
            }
            else
                return false;
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(TestSQL.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
            return true;
        }
    }
    //Users
    public String getNameFromLogin(String password, String email) {
        Statement s = getStatement();
        ResultSet rs = null;

        String query = "Select name FROM sql5115417.Users WHERE password = \'" + password + "\' AND email = \'" + email + "\'";
        try {
            rs = s.executeQuery(query);

            // Valid login
            if (rs.next()) {
                return rs.getString(1);
            }
            // Invalid login
            else
                return null;


        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Database.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        return null;
    }

    //Items
    public void markPaid (String email, String itemId) {
        Statement s = getStatement();
        ResultSet rs = null;
        String query = "UPDATE sql5115417.Items SET paid = 1 WHERE email= \"" + email + "\" AND item_id = \"" + itemId + "\";";
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Database.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    //Groups
    public void addUserToGroup (String email, String groupID    ) {
        Statement s = getStatement();
        ResultSet rs = null;


        String query = "CREATE TABLE sql5115417.temp AS SELECT * FROM sql5115417.Groups WHERE group_id = \"" + groupID + "\" LIMIT 1;";
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Database.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        System.out.println(query);


        query = "UPDATE sql5115417.temp SET email = \"" + email + "\";";
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Database.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        System.out.println(query);


        query = "INSERT INTO sql5115417.Groups SELECT * from temp;";
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Database.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        System.out.println(query);


        query = "DROP TABLE sql5115417.temp;";
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Database.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        System.out.println(query);
    }

    //Groups
    public void removeUserFromGroup (String email, String groupID    ) {
        Statement s = getStatement();
        ResultSet rs = null;

        String query = "DELETE FROM sql5115417.Groups WHERE group_id = \"" + groupID + "\"  AND email = \"" + email + "\";";
        try {
            s.executeUpdate(query);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Database.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        System.out.println(query);
    }

    //groups
    public ArrayList<User> getUsersinGroup (String groupID    ) {
        Statement s = getStatement();
        ResultSet rs = null;
        ArrayList<User> users = new ArrayList<User>();
        String query = "select name,email from sql5115417.Users where email in (Select email from sql5115417.Groups where group_id = \"" + groupID + "\");";
        try {
            rs = s.executeQuery(query);

            if(rs == null) {
                System.err.println("No users of " + groupID + " exists");
            }

            while(rs.next()) {
//                Split split = new Split(rs.getDouble(2), rs.getString(9), rs.getBoolean(6));
//                splits.add(split);
                User user = new User(rs.getString("name"), rs.getString("email"), "");
                users.add(user);
            }


        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Database.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
        System.out.println(query);
        return users;
    }
}
