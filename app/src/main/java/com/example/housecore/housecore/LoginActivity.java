package com.example.housecore.housecore;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    EditText email, password;
    CheckBox rememberMeBox;
    private EmailValidator emailValidator;
    private String user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        emailValidator = new EmailValidator();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginCredentialsManager lcmMyGroups = new LoginCredentialsManager(getApplicationContext());
        if((lcmMyGroups.getCredentials() != null) && (lcmMyGroups.getRememberMe())){
            Intent SkipLoginIntent = new Intent(this, MyGroups.class);
            startActivity(SkipLoginIntent);
        }
    }

    /*
    Leads the user to the groups page if user has valid credentials.
    Method is evoked when the Login button is clicked.
    */
    public void login(View view) {
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        rememberMeBox=(CheckBox)findViewById(R.id.rememberMe);

        //if form is valid, proceed to login
        if(isFormValid()){
            if(isLoginValid(email.getText().toString(), password.getText().toString())) {
                Intent myGroupsIntent = new Intent(this, MyGroups.class);
                LoginCredentialsManager lcm = new LoginCredentialsManager(getApplicationContext());
                lcm.setCredentials(email.getText().toString(), password.getText().toString(), rememberMeBox.isChecked());
                startActivity(myGroupsIntent);
            } else {
                email.setError("Invalid login information");
            }
        }
    }

    //Switches to the create account activity
    public void createAccount(View view){
        Intent intent = new Intent(this, CreateAccountActivity.class);
        startActivity(intent);
    }

    //checks if the login form is valid
    private boolean isFormValid(){

        boolean valid = true;
        //form handling
        if(!isEmailValid(email.getText().toString())) {
            email.setError(getResources().getString(R.string.error_invalid_email));
            valid = false;
        }
        if(email.getText().toString().trim().length() == 0 ) {
            email.setError(getResources().getString(R.string.error_field_required));
            valid = false;
        }
        if(!isPasswordValid(password.getText().toString())) {
            password.setError(getResources().getString(R.string.error_invalid_password));
            valid = false;
        }
        if(password.getText().toString().trim().length() == 0) {
            password.setError(getResources().getString(R.string.error_field_required));
            valid = false;
        }
        return valid;
    }
    private boolean isEmailValid(String email) {
        return emailValidator.isValidEmailAddress(email);
    }
    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return true;
        //return password.length() > 4;
    }

    //Puts username into private string user if successful
    private boolean isLoginValid(String em, String p) {
        class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
            private Database d = new Database();

            private String result;
            @Override
            protected Void doInBackground(String... params) {
                user = d.getNameFromLogin(params[1], params[0]);
                return null;
            }

        };
        AsyncTaskRunner a = new AsyncTaskRunner();
        try {
            a.execute(em, p).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return user != null;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}

