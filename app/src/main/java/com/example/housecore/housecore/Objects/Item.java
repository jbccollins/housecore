package com.example.housecore.housecore.Objects;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Orest on 4/19/2016.
 */
public class Item {
    public String creatorEmail;
    public String creatorName;
    public Double amountTotal;
    public String itemID;
    public String itemName;
    public String groupId;
    public Date dateCreated;
    public Date dateDue;
    public boolean allPaid;


    public ArrayList<Split> splits;

    //objects only made going from model to backend/content
    public Item(String itemName, String creatorEmail, Date dateDue, ArrayList<Split> splits, String groupId, String itemID){
        this.creatorEmail = creatorEmail;
        this.amountTotal = 0.0;
        allPaid = true;
        for(Split split: splits){
            amountTotal+= split.splitAmount;
            if(split.paid == false){
                allPaid = false;
            }
        }
        this.splits = splits;
        this.groupId = groupId;
        this.dateCreated = new Date();
        this.itemName = itemName;
        this.dateDue = dateDue;
        this.itemID = itemID;
    }

    public void setCreatorName(String creatorName){
        this.creatorName = creatorName;
    }
}
