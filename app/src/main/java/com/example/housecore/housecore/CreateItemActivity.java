package com.example.housecore.housecore;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.housecore.housecore.Objects.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.UUID;

public class CreateItemActivity extends AppCompatActivity implements View.OnClickListener {

    ListView listv;
    String itemName;
    String dueByS;
    ListView paySplits;
    ArrayList<Double> paySplitsAmounts;
    ArrayList<String> peopleInSplit;
    ArrayList<User> users;
    LoginCredentials loginCredentials;
    EditText dueByEditText;
    String groupID;
    private DatePickerDialog DPD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_item);

        dueByEditText = (EditText) findViewById(R.id.dueBy);
        dueByEditText.setInputType(InputType.TYPE_NULL);
        setDateTimeField();
        groupID = getIntent().getExtras().getString("groupID");
//        totalAmount = Float.parseFloat(((EditText) findViewById(R.id.itemName)).getText().toString());
        LoginCredentialsManager lcm = new LoginCredentialsManager(getApplicationContext());
        loginCredentials = lcm.getCredentials();
        peopleInSplit = new ArrayList<String>();
        getUsersInGroup();


        for(User u : users){
            // Can't charge yourself
            if(u.getEmail().equals(loginCredentials.email)){
                continue;
            }
            peopleInSplit.add(u.getName());
        }


        MyAdapter adapter = new MyAdapter(this, R.layout.split_row, peopleInSplit);
        listv = (ListView) findViewById(R.id.splits);
        listv.setAdapter(adapter);


    }

    public void createItem(View view){

        EditText itemNameTextView = (EditText) findViewById(R.id.itemName);
        EditText dueByTextView = (EditText) findViewById((R.id.dueBy));
        itemName = ((EditText) findViewById(R.id.itemName)).getText().toString();
        dueByS = ((EditText) findViewById(R.id.dueBy)).getText().toString();
        paySplits = ((ListView) findViewById(R.id.splits));
        paySplitsAmounts = new ArrayList<Double>();
        if(itemName.equals("")){
            itemNameTextView.setError("Item name missing");
            return;
        }
        if(dueByS.equals("")){
            dueByTextView.setError("Item Due by missing");
            return;
        }


        double total_amount = 0;
        Log.v("numChild", Integer.toString(paySplits.getChildCount()));
        for(int i = 0; i < paySplits.getChildCount(); i++){
            Log.v("In forloop", "now");
            View a = paySplits.getChildAt(i);
            EditText editTextSplit =(EditText) a.findViewById(i);
            String amount = editTextSplit.getText().toString();

            if(amount.equals("")){
                Log.v("in if", ".");
                amount = "0";
                Log.v("current amount", amount);
            }
            else{
                Log.v("else", "k");
                Log.v("current amount", amount);
            }

            paySplitsAmounts.add(Double.parseDouble(amount));
            total_amount += Double.parseDouble(amount);
        }

        Log.v("total_amount", Double.toString(total_amount));
        if(total_amount == 0){
            itemNameTextView.setError("Cannot create empty item");
            return;
        }
        UUID itemID = UUID.randomUUID();

        ArrayList<String> userEmails = new ArrayList<String>();
        ArrayList<Double> amounts = new ArrayList<Double>();
        ArrayList<User> goodUsers = new ArrayList<User>();
        for(User u: users){
            if(!u.getEmail().equals(loginCredentials.email)){
                goodUsers.add(u);
            }
        }
        users = goodUsers;
        Log.v("Userssize", String.valueOf(users.size()));
        for(int i = 0; i < users.size(); i++){
            userEmails.add(users.get(i).getEmail());
            Log.v("currUserEma",userEmails.get(i));
            amounts.add(paySplitsAmounts.get(i));
        }
        addItemIntoDB(userEmails, amounts, itemID.toString());
        setResult(RESULT_OK);
        finish();
    }



    private class MyAdapter extends ArrayAdapter<String> {

        Context mContext;
        int layoutResourceId;
        ArrayList<String> data = null;

        public MyAdapter(Context mContext, int layoutResourceId, ArrayList<String> data) {

            super(mContext, layoutResourceId, data);

            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.data = data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
        /*
         * The convertView argument is essentially a "ScrapView" as described is Lucas post
         * http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
         * It will have a non-null value when ListView is asking you recycle the row layout.
         * So, when convertView is not null, you should simply update its contents instead of inflating a new row layout.
         */
            if(convertView==null){
                // inflate the layout
                holder = new ViewHolder();
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
                holder.caption = (EditText) convertView.findViewById(R.id.person_id);
                convertView.setTag(holder);
            }
            else{
                holder = (ViewHolder) convertView.getTag();
            }

            // object item based on the position
            String person = data.get(position);
            holder.caption.setId(position);
            // get the TextView and then set the text (item name) and tag (item ID) values
            TextView textViewItem = (TextView) convertView.findViewById(R.id.person_name);
            textViewItem.setText(person);

            return convertView;

        }

    }

    private void getUsersInGroup(){
        class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
            private Database d = new Database();
            @Override
            protected Void doInBackground(String... params) {
                users = d.getUsersinGroup(params[0]);
                return null;
            }
        }
        AsyncTaskRunner a = new AsyncTaskRunner();
        try {
            a.execute(groupID).get();
            Iterator<User> userIterator = users.iterator();
            while(userIterator.hasNext()){
                Log.v("User Name", userIterator.next().getName());
            }
        } catch (Exception e) {
            Log.v("MY_GROUPS", "Failed to retrieve users");
        }
    }



    private void addItemIntoDB(final ArrayList<String> userEmails, final ArrayList<Double> amount, final String itemID){
        class AsyncTaskRunner extends AsyncTask<String, Void, Void> {
            private Database d = new Database();
            public Boolean result;
            @Override
            protected Void doInBackground(String... params) {
                try {
                    Date date;
                    DateFormat format = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
                    date = format.parse(dueByS);
                    java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                    d.addMultipleItems(loginCredentials.email, amount, itemName, userEmails, groupID, false, itemID, sqlDate.toString());
//                    d.addItem(loginCredentials.email, amount, itemName, u.getEmail(), "personGroupID", false, itemID, sqlDate.toString());
                    return null;
                }
                catch (Exception e){
                    Log.v("Date", "Date conversion fail");
//                    Log.v("dueByS", dueByS);
                }
                return null;
            }

        };
        AsyncTaskRunner a = new AsyncTaskRunner();
        try {
            a.execute().get();

        } catch (Exception e) {
            Log.v("ItemCreation", "Failed to insert split");
        }

    }

    class ViewHolder{
        EditText caption;
    }

    private void setDateTimeField() {
        dueByEditText.setOnClickListener(this);

        final DateFormat sdformat = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
        Calendar newCalendar = Calendar.getInstance();
        DPD = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dueByEditText.setError(null);
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dueByEditText.setText(sdformat.format(newDate.getTime()).toString());
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    }


    public void openDialog(String errorMsg) {
        final Dialog dialog = new Dialog(this); // Context, this, etc.
        dialog.setContentView(R.layout.error_box);
        dialog.setTitle("Error");
        TextView box = (TextView) dialog.findViewById(R.id.box);
        box.setText(errorMsg);
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        if(view == dueByEditText) {
            Log.v("Edittext", "clicked");
            DPD.show();
        }
    }


}

